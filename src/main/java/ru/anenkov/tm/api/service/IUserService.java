package ru.anenkov.tm.api.service;

import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.enumeration.Role;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeUser(User user);

    User removeById(String id);

    User removeByLogin(String login);

    User removeByEmail(String email);

    User updateUserFirstName(String userId, String newFirstName);

    User updateUserMiddleName(String userId, String newMiddleName);

    User updateUserLastName(String userId, String newLastName);

    User updateUserEmail(String userId, String newEmail);

    User updatePassword(String userId, String newPassword);

    User lockUserByLogin (String login);

    User UnlockUserByLogin(String login);

    User DeleteUserByLogin(String login);

}
