package ru.anenkov.tm.api.service;

import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.enumeration.Role;

public interface IAuthService {

    User showUserProfile();

    String getUserId();

    boolean isAuth();

    void checkRole(Role[] roles);

    void logout();

    void login(String login, String password);

    void registry(String login, String password, String email);

    void updatePassword(String newPassword);

    void updateUserFirstName(String newFirstName);

    void updateUserMiddleName(String newMiddleName);

    void updateUserLastName(String newLastName);

    void updateUserEmail(String newEmail);

}
