package ru.anenkov.tm.api.repository;

import ru.anenkov.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {

    void add(String userId, Task task);

    void remove(String userId, Task task);

    List<Task> findAll(String userId);

    void clear(String userId);

    Task findOneByIndex(String userId, Integer index);

    Task findOneByName(String userId, String name);

    Task findOneById(String userId, String id);

    Task removeOneByIndex(String userId, Integer index);

    Task removeOneByName(String userId, String name);

    Task removeOneById(String userId, String id);

}
