package ru.anenkov.tm.api.repository;

import ru.anenkov.tm.entity.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User add(User user);

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeByLogin(String login);

    User removeUser(User user);

    User removeById(String id);

    User removeByEmail(String email);

}
