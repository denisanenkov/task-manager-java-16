package ru.anenkov.tm.entity;

import java.util.UUID;

public class Project extends AbstractEntity {

    private String name;

    private String description;

    private String userId;

    public String getUserId() { return userId; }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return getId() + ": " + name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}