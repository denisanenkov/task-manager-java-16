package ru.anenkov.tm.service;

import ru.anenkov.tm.api.repository.IUserRepository;
import ru.anenkov.tm.api.service.IUserService;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.exception.empty.*;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.HashUtil;

import javax.jws.soap.SOAPBinding;
import java.util.List;

public class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public User findByEmail(String email) {
        return null;
    }

    @Override
    public User removeUser(final User user) {
        if (user == null) return null;
        return userRepository.removeUser(user);
    }

    @Override
    public User removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.removeById(id);
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeByLogin(login);
    }

    @Override
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        User user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setRole(Role.USER);
        return userRepository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setRole(Role.USER);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    @Override
    public User removeByEmail(final String email) {
        if (email == null || email.isEmpty()) return null;
        return userRepository.removeByEmail(email);
    }

    @Override
    public User updateUserFirstName(final String userId, final String newFirstName) {
        if (userId == null || userId.isEmpty()) return null;
        if (newFirstName == null || newFirstName.isEmpty()) return null;
        User user = findById(userId);
        user.setFirstName(newFirstName);
        return user;
    }

    @Override
    public User updateUserMiddleName(final String userId, final String newMiddleName) {
        if (userId == null || userId.isEmpty()) return null;
        if (newMiddleName == null || newMiddleName.isEmpty()) return null;
        User user = findById(userId);
        user.setMiddleName(newMiddleName);
        return user;
    }

    @Override
    public User updateUserLastName(final String userId, final String newLastName) {
        if (userId == null || userId.isEmpty()) return null;
        if (newLastName == null || newLastName.isEmpty()) return null;
        User user = findById(userId);
        user.setLastName(newLastName);
        return user;
    }

    @Override
    public User updateUserEmail(final String userId, final String newEmail) {
        if (userId == null || userId.isEmpty()) return null;
        if (newEmail == null || newEmail.isEmpty()) return null;
        User user = findById(userId);
        user.setEmail(newEmail);
        return user;
    }

    @Override
    public User updatePassword(final String userId, final String newPassword) {
        if (userId == null || userId.isEmpty()) return null;
        if (newPassword == null || newPassword.isEmpty()) return null;
        final User user = findById(userId);
        user.setPasswordHash(HashUtil.salt(newPassword));
        return user;
    }

    @Override
    public User lockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = findByLogin(login);
        if (user == null) return null;
        user.setLocked(true);
        return user;
    }

    @Override
    public User UnlockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = findByLogin(login);
        if (user == null) return null;
        user.setLocked(false);
        return user;
    }

    @Override
    public User DeleteUserByLogin(final String login) {
        if (login == null || login.isEmpty()) return null;
        final User user = findByLogin(login);
        if (user == null) return null;
        userRepository.removeUser(user);
        return user;
    }

}
