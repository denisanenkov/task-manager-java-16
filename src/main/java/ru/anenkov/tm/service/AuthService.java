package ru.anenkov.tm.service;

import ru.anenkov.tm.api.service.IAuthService;
import ru.anenkov.tm.api.service.IUserService;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.exception.empty.EmptyLoginException;
import ru.anenkov.tm.exception.empty.EmptyPasswordException;
import ru.anenkov.tm.exception.user.AccessDeniedException;
import ru.anenkov.tm.util.HashUtil;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public void checkRole(Role[] roles) {
        if (roles == null || roles.length == 0) return;
        final String userId = getUserId();
        final User user = userService.findById(userId);
        final Role role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        for (final Role item: roles) {
            if (role.equals(item)) {
                return;
            }
            throw new AccessDeniedException();
        }
    }

    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user.isLocked()) throw new AccessDeniedException();
        if (user == null) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void registry(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (email == null || email.isEmpty()) return;
        userService.create(login, password, email);
    }

    @Override
    public void updatePassword(final String newPassword) {
        if (!isAuth()) throw new AccessDeniedException();
        userService.updateUserFirstName(userId, newPassword);
    }

    @Override
    public User showUserProfile() {
        if (!isAuth()) throw new AccessDeniedException();
        return userService.findById(userId);
    }

    @Override
    public void updateUserFirstName(final String newFirstName) {
        if (!isAuth()) throw new AccessDeniedException();
        if (newFirstName == null || newFirstName.isEmpty()) return;
        userService.updateUserFirstName(userId, newFirstName);
    }

    @Override
    public void updateUserMiddleName(final String newMiddleName) {
        if (!isAuth()) throw new AccessDeniedException();
        if (newMiddleName == null || newMiddleName.isEmpty()) return;
        userService.updateUserMiddleName(userId, newMiddleName);
    }

    @Override
    public void updateUserLastName(final String newLastName) {
        if (!isAuth()) throw new AccessDeniedException();
        if (newLastName == null || newLastName.isEmpty()) return;
        userService.updateUserLastName(userId, newLastName);
    }

    @Override
    public void updateUserEmail(final String newEmail) {
        if (!isAuth()) throw new AccessDeniedException();
        if (newEmail == null || newEmail.isEmpty()) return;
        userService.updateUserFirstName(userId, newEmail);
    }

}
