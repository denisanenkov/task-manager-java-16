package ru.anenkov.tm.bootstrap;

import ru.anenkov.tm.api.repository.ICommandRepository;
import ru.anenkov.tm.api.repository.IProjectRepository;
import ru.anenkov.tm.api.repository.ITaskRepository;
import ru.anenkov.tm.api.repository.IUserRepository;
import ru.anenkov.tm.api.service.*;
import ru.anenkov.tm.command.*;
import ru.anenkov.tm.command.auth.*;
import ru.anenkov.tm.command.project.*;
import ru.anenkov.tm.command.project.removal.ProjectClearCommand;
import ru.anenkov.tm.command.project.removal.ProjectRemoveByIdCommand;
import ru.anenkov.tm.command.project.removal.ProjectRemoveByIndexCommand;
import ru.anenkov.tm.command.project.removal.ProjectRemoveByNameCommand;
import ru.anenkov.tm.command.project.view.ProjectShowCommand;
import ru.anenkov.tm.command.project.view.ProjectShowByIdCommand;
import ru.anenkov.tm.command.project.view.ProjectShowByIndexCommand;
import ru.anenkov.tm.command.project.view.ProjectShowByNameCommand;
import ru.anenkov.tm.command.project.update.ProjectUpdateByIdCommand;
import ru.anenkov.tm.command.project.update.ProjectUpdateByIndexCommand;
import ru.anenkov.tm.command.system.*;
import ru.anenkov.tm.command.task.*;
import ru.anenkov.tm.command.task.removal.TaskRemoveByIdCommand;
import ru.anenkov.tm.command.task.removal.TaskRemoveByIndexCommand;
import ru.anenkov.tm.command.task.removal.TaskRemoveByNameCommand;
import ru.anenkov.tm.command.task.removal.TaskClearCommand;
import ru.anenkov.tm.command.task.view.TaskShowByIdCommand;
import ru.anenkov.tm.command.task.view.TaskShowByIndexCommand;
import ru.anenkov.tm.command.task.view.TaskShowByNameCommand;
import ru.anenkov.tm.command.task.view.TaskShowCommand;
import ru.anenkov.tm.command.task.update.TaskUpdateByIdCommand;
import ru.anenkov.tm.command.task.update.TaskUpdateByIndexCommand;
import ru.anenkov.tm.command.user.*;
import ru.anenkov.tm.command.user.update.*;
import ru.anenkov.tm.constant.MessageConst;
import ru.anenkov.tm.exception.system.IncorrectCommandException;
import ru.anenkov.tm.repository.CommandRepository;
import ru.anenkov.tm.repository.ProjectRepository;
import ru.anenkov.tm.repository.TaskRepository;
import ru.anenkov.tm.repository.UserRepository;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.service.*;
import ru.anenkov.tm.util.TerminalUtil;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class Bootstrap implements ServiceLocator {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    {
        registry(new HelpCommand());
        registry(new SystemInfoCommand());
        registry(new LoginCommand());
        registry(new LogoutCommand());
        registry(new RegisterCommand());
        registry(new UserLockCommand());
        registry(new UserUnlockCommand());
        registry(new UserProfileViewCommand());
        registry(new UserPasswordUpdateCommand());
        registry(new UserUpdateEmailCommand());
        registry(new UserUpdateFirstNameCommand());
        registry(new UserUpdateMiddleNameCommand());
        registry(new UserUpdateLastNameCommand());
        registry(new VersionCommand());
        registry(new AboutCommand());
        registry(new ArgumentsViewCommand());
        registry(new CommandsViewCommand());
        registry(new TaskShowCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new ProjectShowCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new UserDeleteCommand());
        registry(new ExitCommand());
    }

    private void registry(final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commands.put(command.name(), command);
    }

    private void initUsers() {
        userService.create("1", "1", "test@mail.ru");
        userService.create("test", "test", "test@mail.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    private void parseCommand(final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new IncorrectCommandException(String.valueOf(command));
        final Role[] roles = command.roles();
        authService.checkRole(command.roles());
        command.execute();
    }



    public void run(final String[] args) {
        System.out.println(MessageConst.WELCOME);
        if (parseArgs(args)) System.exit(0);
        initUsers();
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
    }

    public boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        return true;
    }

    public IUserService getUserService() {
        return userService;
    }

    public IAuthService getAuthService() {
        return authService;
    }

    public ICommandService getCommandService() {
        return commandService;
    }

    public ITaskService getTaskService() {
        return taskService;
    }

    public IProjectService getProjectService() {
        return projectService;
    }

    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

}