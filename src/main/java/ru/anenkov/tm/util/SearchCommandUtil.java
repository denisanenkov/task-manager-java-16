package ru.anenkov.tm.util;

import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.exception.system.IncorrectCommandException;
import ru.anenkov.tm.repository.CommandRepository;

import java.util.List;

public interface SearchCommandUtil {

    static void commandFind(String arg) {
        boolean isFinded = false;
        final CommandRepository commandRepository = new CommandRepository();
        final List<AbstractCommand> commands = commandRepository.getCommands();
        if (commands == null) return;
        for (AbstractCommand command : commands) {
            if (command.name().equals(arg)) isFinded = true;
        }
        if (!isFinded) throw new IncorrectCommandException(arg);
    }

}