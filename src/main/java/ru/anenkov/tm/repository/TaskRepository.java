package ru.anenkov.tm.repository;

import ru.anenkov.tm.api.repository.ITaskRepository;
import ru.anenkov.tm.exception.empty.EmptyIdException;
import ru.anenkov.tm.exception.system.IncorrectIndexException;
import ru.anenkov.tm.exception.empty.EmptyNameException;
import ru.anenkov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final String userId, final Task task) {
        task.setUserId(userId);
        if (task == null) return;
        tasks.add(task);
    }

    @Override
    public List<Task> findAll(final String userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void clear(final String userId) {
        final List<Task> tasks = findAll(userId);
        this.tasks.removeAll(tasks);
    }

    @Override
    public Task findOneByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        final List<Task> result = new ArrayList<>();
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result.get(index);
    }

    @Override
    public Task findOneByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final List<Task> result = new ArrayList<>();
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        for (final Task task : result) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task findOneById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final List<Task> result = new ArrayList<>();
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        for (final Task task : result) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public void remove(final String userId, final Task task) {
        if (!userId.equals(task.getUserId())) return;
        if (task == null) return;
        this.tasks.remove(task);
    }

    @Override
    public Task removeOneByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeOneByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(userId, name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeOneById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

}
