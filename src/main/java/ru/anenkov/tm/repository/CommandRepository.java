package ru.anenkov.tm.repository;

import ru.anenkov.tm.api.repository.ICommandRepository;
import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.command.auth.*;
import ru.anenkov.tm.command.project.ProjectCreateCommand;
import ru.anenkov.tm.command.project.removal.ProjectClearCommand;
import ru.anenkov.tm.command.project.removal.ProjectRemoveByIdCommand;
import ru.anenkov.tm.command.project.removal.ProjectRemoveByIndexCommand;
import ru.anenkov.tm.command.project.removal.ProjectRemoveByNameCommand;
import ru.anenkov.tm.command.project.view.ProjectShowCommand;
import ru.anenkov.tm.command.project.view.ProjectShowByIdCommand;
import ru.anenkov.tm.command.project.view.ProjectShowByIndexCommand;
import ru.anenkov.tm.command.project.view.ProjectShowByNameCommand;
import ru.anenkov.tm.command.project.update.ProjectUpdateByIdCommand;
import ru.anenkov.tm.command.project.update.ProjectUpdateByIndexCommand;
import ru.anenkov.tm.command.system.*;
import ru.anenkov.tm.command.task.TaskCreateCommand;
import ru.anenkov.tm.command.task.removal.TaskRemoveByIdCommand;
import ru.anenkov.tm.command.task.removal.TaskRemoveByIndexCommand;
import ru.anenkov.tm.command.task.removal.TaskRemoveByNameCommand;
import ru.anenkov.tm.command.task.removal.TaskClearCommand;
import ru.anenkov.tm.command.task.view.TaskShowByIdCommand;
import ru.anenkov.tm.command.task.view.TaskShowByIndexCommand;
import ru.anenkov.tm.command.task.view.TaskShowByNameCommand;
import ru.anenkov.tm.command.task.view.TaskShowCommand;
import ru.anenkov.tm.command.task.update.TaskUpdateByIdCommand;
import ru.anenkov.tm.command.task.update.TaskUpdateByIndexCommand;
import ru.anenkov.tm.command.user.UserProfileViewCommand;
import ru.anenkov.tm.command.user.update.*;

import java.util.ArrayList;
import java.util.List;

public class CommandRepository implements ICommandRepository {

    private final List<AbstractCommand> commandList = new ArrayList<>();

    {
        commandList.add(new HelpCommand());
        commandList.add(new SystemInfoCommand());
        commandList.add(new LoginCommand());
        commandList.add(new LogoutCommand());
        commandList.add(new UserDeleteCommand());
        commandList.add(new RegisterCommand());
        commandList.add(new UserProfileViewCommand());
        commandList.add(new UserPasswordUpdateCommand());
        commandList.add(new UserUpdateEmailCommand());
        commandList.add(new UserUpdateFirstNameCommand());
        commandList.add(new UserUpdateMiddleNameCommand());
        commandList.add(new UserUpdateLastNameCommand());
        commandList.add(new VersionCommand());
        commandList.add(new AboutCommand());
        commandList.add(new ArgumentsViewCommand());
        commandList.add(new CommandsViewCommand());
        commandList.add(new TaskShowCommand());
        commandList.add(new TaskClearCommand());
        commandList.add(new TaskCreateCommand());
        commandList.add(new TaskShowByIndexCommand());
        commandList.add(new TaskShowByIdCommand());
        commandList.add(new TaskShowByNameCommand());
        commandList.add(new TaskUpdateByIndexCommand());
        commandList.add(new TaskUpdateByIdCommand());
        commandList.add(new TaskRemoveByIdCommand());
        commandList.add(new TaskRemoveByIndexCommand());
        commandList.add(new TaskRemoveByNameCommand());
        commandList.add(new ProjectShowCommand());
        commandList.add(new ProjectClearCommand());
        commandList.add(new ProjectCreateCommand());
        commandList.add(new ProjectShowByIndexCommand());
        commandList.add(new ProjectShowByIdCommand());
        commandList.add(new ProjectShowByNameCommand());
        commandList.add(new ProjectUpdateByIndexCommand());
        commandList.add(new ProjectUpdateByIdCommand());
        commandList.add(new ProjectRemoveByIdCommand());
        commandList.add(new ProjectRemoveByIndexCommand());
        commandList.add(new ProjectRemoveByNameCommand());
        commandList.add(new ExitCommand());
    }

    @Override
    public List<AbstractCommand> getCommands() {
        return commandList;
    }

    @Override
    public List<AbstractCommand> getCommandList() {
        return commandList;
    }

}