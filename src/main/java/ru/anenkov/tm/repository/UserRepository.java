package ru.anenkov.tm.repository;

import ru.anenkov.tm.api.repository.IUserRepository;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.exception.user.AccessDeniedException;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private final List<User> users = new ArrayList<>();

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public User add(final User user) {
        users.add(user);
        return user;
    }

    @Override
    public User findById(final String id) {
        for (final User user : users) {
            if (user.getId().equals(id)) { return user; }
        }
        return null;
    }

    @Override
    public User findByLogin(final String login) {
        for (final User user : users) {
            if (user.getLogin().equals(login)) { return user; }
        }
        throw new AccessDeniedException();
    }

    @Override
    public User removeById(final String id) {
        final User user = findById(id);
        if (user == null) return null;
        removeUser(user);
        return user;
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        removeUser(user);
        return user;
    }

    @Override
    public User removeUser(final User user) {
        users.remove(user);
        return user;
    }

    @Override
    public User findByEmail(final String email) {
        for (User user : users) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public User removeByEmail(final String email) {
        final User user = findByEmail(email);
        if (user == null) return null;
        users.remove(user);
        return removeUser(user);
    }

}
