package ru.anenkov.tm.command.system;

import ru.anenkov.tm.bootstrap.Bootstrap;
import ru.anenkov.tm.command.AbstractCommand;

import java.util.Collection;

public class HelpCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public String name() {
        return "Help";
    }

    @Override
    public String description() {
        return "Display terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("[Help]");
        final Bootstrap bootstrap = (Bootstrap) serviceLocator;
        final Collection<AbstractCommand> commands = bootstrap.getCommands();
        for (AbstractCommand command: commands) {
            System.out.println(command.name() + " - " + command.description());
        }
        System.out.println("[OK]");
    }

}
