package ru.anenkov.tm.command.system;

import ru.anenkov.tm.command.AbstractCommand;

public class ExitCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "Exit";
    }

    @Override
    public String description() {
        return "Exit";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
