package ru.anenkov.tm.command.system;

import ru.anenkov.tm.api.service.ServiceLocator;
import ru.anenkov.tm.bootstrap.Bootstrap;
import ru.anenkov.tm.command.AbstractCommand;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class ArgumentsViewCommand extends AbstractCommand {

    @Override
    public final String arg() {
        return "-arg";
    }

    @Override
    public String name() {
        return "Arguments";
    }

    @Override
    public String description() {
        return "Show program arguments.";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        final List<AbstractCommand> commands = serviceLocator.getCommandService().getCommandList();
        for (AbstractCommand command: commands) {
            if (command.arg() != null) {
                System.out.println(command.arg());
            }
        }
    }

}
