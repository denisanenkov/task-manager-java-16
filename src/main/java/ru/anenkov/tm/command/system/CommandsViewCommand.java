package ru.anenkov.tm.command.system;

import ru.anenkov.tm.command.AbstractCommand;

import java.util.List;

public class CommandsViewCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-com";
    }

    @Override
    public String name() {
        return "Commands";
    }

    @Override
    public String description() {
        return "Show program commands.";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        final List<AbstractCommand> commands = serviceLocator.getCommandService().getCommandList();
        for (AbstractCommand command: commands) System.out.println(command.name());
    }

}
