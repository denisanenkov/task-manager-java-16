package ru.anenkov.tm.command.auth;

import ru.anenkov.tm.command.AbstractCommand;

public class LogoutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "Logout";
    }

    @Override
    public String description() {
        return "Logout user";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        serviceLocator.getAuthService().logout();
        System.out.println("[OK]");
    }

}
