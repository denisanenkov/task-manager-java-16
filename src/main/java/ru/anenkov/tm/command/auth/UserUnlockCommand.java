package ru.anenkov.tm.command.auth;

import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

public class UserUnlockCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "Unlock-user";
    }

    @Override
    public String description() {
        return "Unlock user";
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("[ENTER LOGIN]");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().UnlockUserByLogin(login);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
