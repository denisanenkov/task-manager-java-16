package ru.anenkov.tm.command.task.update;

import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.entity.Task;
import ru.anenkov.tm.util.TerminalUtil;

public class TaskUpdateByIndexCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "Task-update-by-index";
    }

    @Override
    public String description() {
        return "Update task - list by index";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = serviceLocator.getAuthService().getUserId();
        final Task task = serviceLocator.getTaskService().findOneByIndex(userId, index);
        if (task == null) { System.out.println("[FAIL]"); return; }
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = serviceLocator.getTaskService().updateTaskByIndex(userId, index, name, description);
        if (taskUpdated == null) { System.out.println("[FAIL]"); return; }
        System.out.println("[OK]");
    }

}
