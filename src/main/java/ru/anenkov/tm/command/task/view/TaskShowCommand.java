package ru.anenkov.tm.command.task.view;

import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.entity.Task;

import java.util.List;

public class TaskShowCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "Task-list";
    }

    @Override
    public String description() {
        return "Show task list";
    }

    @Override
    public void execute() {
        System.out.println("[LIST TASKS]");
        final String userId = serviceLocator.getAuthService().getUserId();
        final List<Task> tasks = serviceLocator.getTaskService().findAll(userId);
        for (Task task : tasks) System.out.println(task);
        System.out.println("[OK]");
    }

}
