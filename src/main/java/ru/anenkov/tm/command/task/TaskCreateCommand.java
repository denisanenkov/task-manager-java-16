package ru.anenkov.tm.command.task;

import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.util.TerminalUtil;

public class TaskCreateCommand extends AbstractCommand {

    @Override
    public final String arg() {
        return null;
    }

    @Override
    public final String name() {
        return "Task-create";
    }

    @Override
    public final String description() {
        return "Create new task";
    }

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String userId = serviceLocator.getAuthService().getUserId();
        final String description = TerminalUtil.nextLine();
        serviceLocator.getTaskService().create(userId, name, description);
        System.out.println("[OK]");
    }

}
