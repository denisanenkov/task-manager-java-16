package ru.anenkov.tm.command.task.removal;

import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.entity.Task;
import ru.anenkov.tm.util.TerminalUtil;

public class TaskRemoveByNameCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "Task-remove-by-name";
    }

    @Override
    public String description() {
        return "Delete task from task - list by name";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final String userId = serviceLocator.getAuthService().getUserId();
        final Task task = serviceLocator.getTaskService().removeOneByName(userId, name);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
