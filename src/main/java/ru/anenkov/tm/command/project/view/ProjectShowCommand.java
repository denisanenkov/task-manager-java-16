package ru.anenkov.tm.command.project.view;

import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.entity.Project;

import java.util.List;

public class ProjectShowCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "Project-list";
    }

    @Override
    public String description() {
        return "Show project list";
    }

    @Override
    public void execute() {
        System.out.println("[LIST PROJECT]");
        final String userId = serviceLocator.getAuthService().getUserId();
        final List<Project> projects = serviceLocator.getProjectService().findAll(userId);
        for (Project project : projects) System.out.println(project);
        System.out.println("[OK]");
    }

}
