package ru.anenkov.tm.command.project.update;

import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.entity.Project;
import ru.anenkov.tm.entity.Task;
import ru.anenkov.tm.util.TerminalUtil;

public class ProjectUpdateByIdCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "Project-update-by-id";
    }

    @Override
    public String description() {
        return "Update project - list by id";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = serviceLocator.getAuthService().getUserId();
        final Project project = serviceLocator.getProjectService().findOneById(userId, id);
        if (project == null) { System.out.println("[FAIL]"); return; }
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = serviceLocator.getProjectService().updateProjectById(userId, id, name, description);
        if (projectUpdated == null) { System.out.println("[FAIL]"); return; }
        System.out.println("[OK]");
    }

}
