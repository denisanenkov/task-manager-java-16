package ru.anenkov.tm.command.project.update;

import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.entity.Project;
import ru.anenkov.tm.entity.Task;
import ru.anenkov.tm.util.TerminalUtil;

public class ProjectUpdateByIndexCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "Project-update-by-index";
    }

    @Override
    public String description() {
        return "Update project - list by index";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = serviceLocator.getAuthService().getUserId();
        final Project project = serviceLocator.getProjectService().findOneByIndex(userId, index);
        if (project == null) { System.out.println("[FAIL]"); return; }
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = serviceLocator.getProjectService().updateProjectByIndex(userId, index, name, description);
        if (projectUpdated == null) { System.out.println("[FAIL]"); return; }
        System.out.println("[OK]");
    }

}
