package ru.anenkov.tm.command.project.removal;

import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.entity.Project;
import ru.anenkov.tm.util.TerminalUtil;

public class ProjectRemoveByIdCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "Project-remove-by-id";
    }

    @Override
    public String description() {
        return "Delete project from project - list by id";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = serviceLocator.getAuthService().getUserId();
        final Project project = serviceLocator.getProjectService().removeOneById(userId, id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
