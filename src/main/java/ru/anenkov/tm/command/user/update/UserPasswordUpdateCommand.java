package ru.anenkov.tm.command.user.update;

import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.util.TerminalUtil;

public class UserPasswordUpdateCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "Update-user-password";
    }

    @Override
    public String description() {
        return "Update user password";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD: ");
        final String newPassword = TerminalUtil.nextLine();
        serviceLocator.getAuthService().updatePassword(newPassword);
        System.out.println("[PASSWORD UPDATED]");
        System.out.println("[OK]");
    }

}
