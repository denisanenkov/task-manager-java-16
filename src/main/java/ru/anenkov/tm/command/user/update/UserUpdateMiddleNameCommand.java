package ru.anenkov.tm.command.user.update;

import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.util.TerminalUtil;

public class UserUpdateMiddleNameCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "Update-middle-name";
    }

    @Override
    public String description() {
        return "Update middle name";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER MIDDLE NAME]");
        System.out.println("ENTER NEW USER MIDDLE NAME: ");
        final String newMiddleName = TerminalUtil.nextLine();
        serviceLocator.getAuthService().updateUserMiddleName(newMiddleName);
        System.out.println("[OK]");
    }

}
