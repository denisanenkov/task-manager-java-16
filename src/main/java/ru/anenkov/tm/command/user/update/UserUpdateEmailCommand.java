package ru.anenkov.tm.command.user.update;

import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.util.TerminalUtil;

public class UserUpdateEmailCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "Update-email";
    }

    @Override
    public String description() {
        return "Update email";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER MAIL]");
        System.out.println("ENTER NEW USER MAIL: ");
        final String newUserEmail = TerminalUtil.nextLine();
        serviceLocator.getAuthService().updateUserEmail(newUserEmail);
        System.out.println("[OK]");
    }

}
