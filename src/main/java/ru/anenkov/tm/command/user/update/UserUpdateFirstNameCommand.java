package ru.anenkov.tm.command.user.update;

import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.util.TerminalUtil;

public class UserUpdateFirstNameCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "Update-first-name";
    }

    @Override
    public String description() {
        return "Update first name";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER FIRST NAME]");
        System.out.println("ENTER NEW USER FIRST NAME: ");
        final String newFirstName = TerminalUtil.nextLine();
        serviceLocator.getAuthService().updateUserFirstName(newFirstName);
        System.out.println("[OK]");
    }

}
